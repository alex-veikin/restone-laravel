<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'cart'], function () {
    Route::post('/', 'CartController@apiIndex');
    Route::post('increase/{id}', 'CartController@apiIncreaseItem');
    Route::post('decrease/{id}', 'CartController@apiDecreaseItem');
    Route::post('remove/{id}', 'CartController@apiRemoveItem');
    Route::post('clear', 'CartController@apiClearCart');
});

Route::group(['prefix' => 'order', 'middleware'=>'auth:api'], function () {
    Route::get('/', 'OrderController@apiStore');
});
