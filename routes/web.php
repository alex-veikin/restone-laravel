<?php

Route::get('/', 'PageController@index')->name('home');

Route::resource('cuisines', 'CuisineController')->only(['index', 'show']);
Route::resource('categories', 'CategoryController')->only(['index', 'show']);
Route::resource('dishes', 'DishController')->only(['index', 'show']);


// Cart
Route::group(['prefix' => 'cart',/* 'middleware' => 'auth',*/ 'as' => 'cart.'], function () {
    Route::get('/', 'CartController@index')->name('index');
    Route::get('{dish}/increase', 'CartController@increaseItem')->name('increase');
    Route::get('{dish}/decrease', 'CartController@decreaseItem')->name('decrease');
    Route::get('{dish}/remove', 'CartController@removeItem')->name('remove');
    Route::get('clear', 'CartController@clearCart')->name('clear');
});


// Order
Route::resource('order', 'OrderController')
    ->middleware('auth')
    ->only(['store', 'show']);


// User Panel
Route::group(['prefix' => 'user', 'middleware' => 'auth', 'as' => 'user.'], function () {
    Route::get('/', 'UserController@userIndex')->name('index');
    Route::get('edit', 'UserController@userEdit')->name('edit');
    Route::post('edit/profile', 'UserController@userUpdateProfile')
        ->name('update.profile');
    Route::post('edit/password', 'UserController@userUpdatePassword')
        ->name('update.password');
//    Route::match(['get', 'post'], 'delete', 'UserController@userDestroy')
//        ->name('delete');
});


// Admin Panel
Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'admin'],
    'as' => 'admin.',
], function () {
    Route::get('/', 'AdminController@index')->name('index');

    Route::get('users', 'UserController@admin')->name('users.index');
    Route::get('users/{user}/delete', 'UserController@destroy')
        ->name('users.delete');
    Route::resource('users', 'UserController',
        ['except' => ['index', 'show']]
    );

    Route::get('pages', 'PageController@admin')->name('pages.index');
    Route::get('pages/{page}/delete', 'PageController@destroy')
        ->name('pages.delete');
    Route::resource('pages', 'PageController',
        ['except' => ['index', 'show']]
    );

    Route::get('cuisines', 'CuisineController@admin')
        ->name('cuisines.index');
    Route::get('cuisines/{cuisine}/delete', 'CuisineController@destroy')
        ->name('cuisines.delete');
    Route::resource('cuisines', 'CuisineController',
        ['except' => ['index', 'show']]
    );

    Route::get('categories', 'CategoryController@admin')->name('categories.index');
    Route::get('categories/{category}/delete', 'CategoryController@destroy')
        ->name('categories.delete');
    Route::resource('categories', 'CategoryController',
        ['except' => ['index', 'show']]
    );

    Route::get('dishes', 'DishController@admin')->name('dishes.index');
    Route::get('dishes/{dish}/delete', 'DishController@destroy')
        ->name('dishes.delete');
    Route::resource('dishes', 'DishController',
        ['except' => ['index', 'show']]
    );

    Route::get('cooks', 'CookController@admin')->name('cooks.index');
    Route::get('cooks/{cook}/delete', 'CookController@destroy')
        ->name('cooks.delete');
    Route::resource('cooks', 'CookController',
        ['except' => ['index', 'show']]
    );

    Route::get('orders', 'OrderController@admin')->name('orders.index');
    Route::resource('orders', 'OrderController',
        ['only' => ['edit', 'update']]
    );

    Route::group(['prefix' => '{nav}-nav', 'as' => 'nav.'], function () {
        Route::get('/', 'NavController@admin')->name('index');
        Route::post('/', 'NavController@store')->name('store');
        Route::get('move/{link}/{direction}', 'NavController@move')->name('move');
        Route::get('delete/{link}', 'NavController@destroy')->name('delete');
        Route::get('edit', 'NavController@navEdit')->name('edit');
        Route::post('edit', 'NavController@navUpdate')->name('update');
    });
});


Auth::routes();


// Other page
Route::get('{alias}', 'PageController@show');