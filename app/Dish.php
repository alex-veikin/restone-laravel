<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{

    protected $fillable =
        [
            'cuisine_id',
            'category_id',
            'title',
            'description',
            'image',
            'price',
        ];

    public function cuisine()
    {
        return $this->belongsTo('App\Cuisine');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
