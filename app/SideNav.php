<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SideNav extends Model
{

    protected $fillable =
        [
            'type',
            'page_id',
            'url',
            'url_title',
            'submenu_title',
            'parent_id',
            'weight',
        ];


    public function page()
    {
        return $this->belongsTo('App\Page');
    }
}
