<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cook extends Model
{

    protected $fillable =
        [
            'weight',
            'alias',
            'full_name',
            'title',
            'description',
            'image',
        ];
}
