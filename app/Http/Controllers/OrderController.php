<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use Illuminate\Http\Request;
use Auth;

class OrderController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $orders = Order::all();

        return view('admin.orders.index', compact('orders'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $user_id = Auth::user()->id;
        $cart = session('cart', null);

        if ($cart) {
            Order::create(['user_id'=>$user_id, 'cart'=>serialize($cart)]);
            Cart::clear();

            return redirect()->route('user.index');
        }

        return redirect()->route('home');
    }

    public function apiStore(Request $request)
    {
        return  $request->user();

//        $user_id = $request->user()->id;
//
//        return $user_id;

//        $user_id = Auth::user()->id;
//        $cart = session('cart', null);
//
//        if ($cart) {
//            Order::create(['user_id'=>$user_id, 'cart'=>serialize($cart)]);
//            Cart::clear();
//
//            return true;
//        }
//
//        return false;
    }


    /**
     * @param \App\Order $order
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Order $order)
    {
        $cart = unserialize($order->cart);

        return view('site.user.order', compact('order', 'cart'));
    }


    /**
     * @param \App\Order $order
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Order $order)
    {
        $cart = unserialize($order->cart);

        return view('admin.orders.edit', compact('order', 'cart'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Order               $order
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Order $order)
    {
        $order->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }
}
