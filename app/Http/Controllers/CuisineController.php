<?php

namespace App\Http\Controllers;

use App\Cuisine;
use Illuminate\Http\Request;
use Image;
use App\Dish;
use App\Http\Requests\CuisineRequest;

class CuisineController extends Controller
{
    /**
     * Вывод списка пунктов кухни в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $cuisines = Cuisine::all();

        return view('admin.cuisines.index', compact('cuisines'));
    }

    /**
     * Все кухни
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cuisines = Cuisine::all();

        return view('site.cuisines.index', compact('cuisines'));
    }

    /**
     * Отображение формы создания новой кухни
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cuisines.create');
    }

    /**
     * Сохранение новой кухни
     *
     * @param \App\Http\Requests\CuisineRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CuisineRequest $request)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(740, 560, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path() . '/img/cuisines/' . $image_name);
        } else {
            $image_name = 'no-image.jpg';
        }

        $request->request->add(['image' => $image_name]);

        Cuisine::create($request->all());

        return redirect()->route('admin.cuisines.index')
                         ->with("success", "Запись успешно создана!");
    }

    /**
     * @param $alias
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($alias)
    {
        $cuisine = Cuisine::where('alias', $alias)->first();
        $dishes = Dish::where('cuisine_id', $cuisine->id)->get();

        $data = [
            'cuisine' => $cuisine,
            'dishes' => $dishes,
        ];

        return view('site.cuisines.show', $data);
    }

    /**
     * Отображение формы редактирования выбранной кухни
     *
     * @param \App\Cuisine $cuisine
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Cuisine $cuisine)
    {
        return view('admin.cuisines.edit', compact('cuisine'));
    }

    /**
     * Обновление выбранной кухни
     *
     * @param \App\Http\Requests\CuisineRequest $request
     * @param \App\Cuisine                      $cuisine
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CuisineRequest $request, Cuisine $cuisine)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(740, 560, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path() . '/img/cuisines/' . $image_name);
        } else {
            $image_name = $cuisine->image;
        }

        $request->request->add(['image' => $image_name]);

        $cuisine->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление выбранной кухни
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Cuisine             $cuisine
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Cuisine $cuisine)
    {
        if ($request->isMethod('delete')) {
            $cuisine->delete();

            return redirect()->route('admin.cuisines.index')
                             ->with("success", "Запись удалена!");
        }

        return view('admin.cuisines.delete', compact('cuisine'));
    }
}
