<?php

namespace App\Http\Controllers;

use App\Page;
use App\Rules\CheckPassword;
use App\Rules\ComparePassword;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Order;

class UserController extends Controller
{

    public function __construct()
    {
        //
    }

    /**
     * Кабинет пользователя.
     *
     * @return \Illuminate\Http\Response
     */
    public function userIndex()
    {
        $user = Auth::user();
        $orders = $user->orders()->where('status', '1')->get();

        return view('site.user.index', compact('user', 'orders'));
    }

    /**
     * Панель редактирования профиля.
     *
     * @return \Illuminate\Http\Response
     */
    public function userEdit()
    {
        $user = Auth::user();

        return view('site.user.edit', compact('user'));
    }

    /**
     * Изменить данные пользователя.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userUpdateProfile(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
            'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
            'password'   => [
                'required',
                'string',
                'min:6',
                new CheckPassword(),
            ],
        ]);

        $user = Auth::user();

        $user->first_name = $request->input('first_name');
        $user->last_name  = $request->input('last_name');

        $user->save();

        return back()->with("success", "Данные успешно изменены!");
    }


    /**
     * Изменить пароль пользователя.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userUpdatePassword(Request $request)
    {
        $this->validate($request, [
            'password_old' => [
                'required',
                new CheckPassword(),
            ],
            'password_new' => [
                'required',
                'string',
                'min:6',
                new ComparePassword(),
                'confirmed',
            ],
        ]);

        $user = Auth::user();

        $user->password = bcrypt($request->input('password_new'));

        $user->save();

        return back()->with("success", "Пароль успешно изменен!");
    }

    /**
     * Удалить аккаунт пользователя.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function userDestroy(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'password' => [
                    'required',
                    new CheckPassword(),
                ],
            ]);

            $user = Auth::user();

            $user->delete();

            return redirect()->route('home');
        }

        return view('site.user.delete');
    }




    ///////////////////////////
    //      Админпанель      //
    ///////////////////////////

    /**
     * Вывод таблицы всех пользователей в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Форма создания нового пользователя в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Сохранение нового пользователя из админки
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
            'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
            'login'      => 'required|alpha_dash|max:255|unique:users',
            'email'      => 'required|email|max:255|unique:users',
            'password'   => 'required|string|min:6|confirmed',
        ]);

        User::create([
            'role'       => $request->input('role'),
            'first_name' => $request->input('first_name'),
            'last_name'  => $request->input('last_name'),
            'login'      => $request->input('login'),
            'email'      => $request->input('email'),
            'password'   => bcrypt($request->input('password')),
        ]);

        return redirect()->route('admin.users.index')
                         ->with("success", "Запись успешно создана!");
    }

    /**
     * Страница редактирования пользователя в админке.
     *
     * @param \App\User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Изменить данные пользователя из админки.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User                $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'role'       => 'required|regex:/(user)?(admin)?/u',
            'first_name' => 'required|regex:/[А-Яа-яЁё]/u|max:60',
            'last_name'  => 'required|regex:/[А-Яа-яЁё]/u|max:60',
            'login'      => 'required|alpha_dash|max:255|unique:users,login,'
                            . $user->id,
            'email'      => 'required|email|max:255|unique:users,email,'
                            . $user->id,
        ]);

        $user->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удалить пользователя из админки.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User                $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, User $user)
    {
        if ($request->isMethod('delete')) {
            $user->delete();

            return redirect()->route('admin.users.index')
                             ->with("success", "Запись удалена!");
        }

        return view('admin.users.delete', compact('user'));
    }
}
