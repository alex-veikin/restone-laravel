<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Illuminate\Http\Request;
use Image;

class CategoryController extends Controller
{
    /**
     * Вывод списка пунктов меню в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Все категории
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::all();

        return view('site.categories.index', compact('categories'));
    }

    /**
     * Отображение формы создания нового раздела меню
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Сохранение нового раздела меню
     *
     * @param \App\Http\Requests\CategoryRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(540, 560, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path() . '/img/categories/' . $image_name);
        } else {
            $image_name = 'no-image.jpg';
        }

        $request->request->add(['image' => $image_name]);

        Category::create($request->all());

        return redirect()->route('admin.categories.index')
                         ->with("success", "Запись успешно создана!");
    }


    /**
     * @param $alias
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($alias)
    {
        $category = Category::where('alias', $alias)->first();
        $dishes = Dish::where('category_id', $category->id)->get();

        $data = [
            'category' => $category,
            'dishes' => $dishes,
        ];

        return view('site.categories.show', $data);
    }

    /**
     * Отображение формы редактирования выбранного раздела меню
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Обновление выбранного раздела меню
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @param \App\Category                          $category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(540, 560, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path() . '/img/categories/' . $image_name);
        } else {
            $image_name = $category->image;
        }

        $request->request->add(['image' => $image_name]);

        $category->update($request->all());

        return redirect()->route('admin.categories.index')
                         ->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление выбранного раздела меню
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Category                $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Category $category)
    {
        if ($request->isMethod('delete')) {
            $category->delete();

            return redirect()->route('admin.categories.index')
                             ->with("success", "Запись удалена!");
        }

        return view('admin.categories.delete', compact('category'));
    }
}
