<?php

namespace App\Http\Controllers;

use App\Cook;
use Illuminate\Http\Request;
use Image;
use App\Http\Requests\CookRequest;

class CookController extends Controller
{
    /**
     * Вывод списка поваров в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $cooks = Cook::all()->sortBy('weight');

        return view('admin.cooks.index', compact('cooks'));
    }

    /**
     *
     */
    public function index()
    {
        //
    }

    /**
     * Отображение формы создания нового повара
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.cooks.create');
    }

    /**
     * Сохранение нового повара
     *
     * @param \App\Http\Requests\CookRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CookRequest $request)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(450, 450, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path() . '/img/cooks/' . $image_name);
        } else {
            $image_name = 'no-image.jpg';
        }

        $request->request->add(['image' => $image_name]);

        Cook::create($request->all());

        return redirect()->route('admin.cooks.index')
                         ->with("success", "Запись успешно создана!");
    }

    /**
     * @param \App\Cook $cook
     */
    public function show(Cook $cook)
    {
        //
    }

    /**
     * Отображение формы редактирования выбранного раздела меню
     *
     * @param \App\Cook $cook
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Cook $cook)
    {
        return view('admin.cooks.edit', compact('cook'));
    }

    /**
     * Обновление выбранного повара
     *
     * @param \App\Http\Requests\CookRequest $request
     * @param \App\Cook                      $cook
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CookRequest $request, Cook $cook)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(450, 450, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path() . '/img/cooks/' . $image_name);
        } else {
            $image_name = $cook->image;
        }

        $request->request->add(['image' => $image_name]);

        $cook->update($request->all());

        return redirect()->route('admin.cooks.index')
                         ->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление выбранного раздела меню
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Cook                $cook
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Cook $cook)
    {
        if ($request->isMethod('delete')) {
            $cook->delete();

            return redirect()->route('admin.cooks.index')
                             ->with("success", "Запись удалена!");
        }

        return view('admin.cooks.delete', compact('cook'));
    }
}
