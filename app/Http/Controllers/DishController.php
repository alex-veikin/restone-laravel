<?php

namespace App\Http\Controllers;

use App\Cuisine;
use App\Category;
use App\Dish;
use Illuminate\Http\Request;
use App\Http\Requests\DishRequest;
use Image;

class DishController extends Controller
{
    /**
     * Вывод списка всех блюд в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $dishes = Dish::all();

        return view('admin.dishes.index', compact('dishes'));
    }

    /**
     * Все блюда
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dishes = Dish::all();

        return view('site.dishes.index', compact('dishes'));
    }

    /**
     * Форма добавления нового блюда
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $cuisines = [];
        foreach (Cuisine::all() as $item) {
            $cuisines[$item->id] = $item->title;
        }

        $categories = [];
        foreach (Category::all() as $item) {
            $categories[$item->id] = $item->title;
        }

        $data = [
            'cuisines'   => $cuisines,
            'categories' => $categories,
        ];

        return view('admin.dishes.create', $data);
    }

    /**
     * Сохранить новое блюдо
     *
     * @param \App\Http\Requests\DishRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DishRequest $request)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(640, 480, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path().'/img/dishes/'.$image_name);
        } else {
            $image_name = 'no-image.jpg';
        }

        $request->request->add(['image' => $image_name]);

        Dish::create($request->all());

        return redirect()->route('admin.dishes.index')
                         ->with("success", "Запись успешно создана!");
    }

    /**
     * Показать выбранное блюдо
     *
     * @param  \App\Dish $dish
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Dish $dish)
    {
        $similar = Dish::where('category_id', $dish->category->id)
                       ->orWhere('cuisine_id', $dish->cuisine->id)
                       ->get()
                       ->where('id', '!=', $dish->id)
                       ->shuffle();

        return view('site.dishes.show', compact('dish', 'similar'));
    }

    /**
     * Форма редактирования блюда
     *
     * @param \App\Dish $dish
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Dish $dish)
    {
        $cuisines = [];
        foreach (Cuisine::all() as $item) {
            $cuisines[$item->id] = $item->title;
        }

        $categories = [];
        foreach (Category::all() as $item) {
            $categories[$item->id] = $item->title;
        }

        $data = [
            'dish'       => $dish,
            'cuisines'   => $cuisines,
            'categories' => $categories,
        ];

        return view('admin.dishes.edit', $data);
    }

    /**
     * Обновить блюдо
     *
     * @param \App\Http\Requests\DishRequest $request
     * @param \App\Dish                      $dish
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DishRequest $request, Dish $dish)
    {
        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $image_name = $image->getClientOriginalName();

            Image::make($image->getRealPath())
                 ->fit(640, 480, function ($constraint) {
                     $constraint->upsize();
                 })
                 ->save(public_path().'/img/dishes/'.$image_name);
        } else {
            $image_name = $dish->image;
        }

        $request->request->add(['image' => $image_name]);

        $dish->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * Удаление блюда
     *
     * @param Request $request
     * @param Dish    $dish
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Dish $dish)
    {
        if ($request->isMethod('delete')) {
            $dish->delete();

            return redirect()->route('admin.dishes.index')
                             ->with("success", "Запись удалена!");
        }

        return view('admin.dishes.delete', compact('dish'));
    }
}
