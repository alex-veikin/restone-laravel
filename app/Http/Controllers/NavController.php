<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\TopNav;
use App\SideNav;

class NavController extends Controller
{

    /**
     * Вывод элементов меню в админке
     *
     * @param $nav
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin($nav)
    {
        $model    = 'App\\'.ucfirst($nav).'Nav';
        $nav_menu = $model::with('page')->get()->sortBy('weight');

        $pages = Page::all();

        $pages_select = ['-выберите страницу-'];
        foreach ($pages as $page) {
            $pages_select[$page->id] = $page->title;
        }

        $parents_select = ['-нет-'];
        foreach ($nav_menu->where('type', 'submenu') as $link) {
            $parents_select[$link->id] = $link->submenu_title;
        }

        $data = [
            'nav'            => $nav,
            'nav_menu'       => $nav_menu,
            'pages_select'   => $pages_select,
            'parents_select' => $parents_select,
        ];

        return view('admin.nav.index', $data);
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $nav
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $nav)
    {
        if ($request->type == 'url') {
            $rules = [
                'url'       => 'required|url',
                'url_title' => 'required|string',
            ];
        } elseif ($request->type == 'page') {
            $rules = ['page_id' => 'required|regex:/[^0]/u'];
        } elseif ($request->type == 'submenu') {
            $rules = ['submenu_title' => 'required|string'];
        }

        $this->validate($request, $rules);

        $model    = 'App\\'.ucfirst($nav).'Nav';
        $nav_menu = $model::all();
        $count    = $nav_menu->where('parent_id', $request->parent_id)->count();

        $data = [
            'type'          => $request->type,
            'page_id'       => null,
            'url'           => null,
            'url_title'     => null,
            'submenu_title' => null,
            'parent_id'     => $request->parent_id,
            'weight'        => ++$count,
        ];

        if ($request->type == 'url') {
            $data['url']       = $request->url;
            $data['url_title'] = $request->url_title;
        } elseif ($request->type == 'page') {
            $data['page_id'] = $request->page_id;
        } elseif ($request->type == 'submenu') {
            $data['submenu_title'] = $request->submenu_title;
        }

        $model::create($data);

        return redirect()
            ->route('admin.nav.index', compact('nav'))
            ->with("success", "Запись успешно создана!");
    }


    /**
     * Перемещение пункта меню вниз/вверх
     *
     * @param $nav
     * @param $id
     * @param $direction
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function move($nav, $id, $direction)
    {
        $model = 'App\\'.ucfirst($nav).'Nav';

        $link = $model::find($id);

        if ($direction == 'up') {
            $weight          = $link->weight - 1;
        } elseif ($direction == 'down') {
            $weight          = $link->weight + 1;
        }

        $link_exchange   = $model::where('parent_id', $link->parent_id)
                                 ->where('weight', $weight)->first();

        $link_exchange->update(['weight' => $link->weight]);
        $link->update(['weight' => $weight]);

        return redirect()
            ->route('admin.nav.index', compact('nav'))
            ->with("success", "Запись успешно перемещена!");
    }


    /**
     * Удаление пункта меню
     *
     * @param $nav
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($nav, $id)
    {
        $model = 'App\\'.ucfirst($nav).'Nav';

        $link = $model::find($id);

        if ($this->deleteMenuItems($model, $link)) {
            $this->shiftMenuItems($model, $link);

            return redirect()
                ->route('admin.nav.index', compact('nav'))
                ->with("success", "Запись успешно удалена!");
        }
    }


    /**
     * Рекурсивное удаление пункта меню и всех его подпунктоа
     *
     * @param $model
     * @param $item
     *
     * @return bool
     */
    private function deleteMenuItems($model, $item)
    {
        $children = $model::where('parent_id', $item->id)->get();

        if ($children->count() > 0) {
            foreach ($children as $link) {
                $this->deleteMenuItems($model, $link);

                $link->delete();
            }
        }

        $item->delete();

        return true;
    }


    /**
     * Смещение номера пунктов меню после удаленного
     *
     * @param $model
     * @param $item
     */
    private function shiftMenuItems($model, $item)
    {
        $items = $model::where('parent_id', $item->parent_id)
                       ->where('weight', '>', $item->weight)
                       ->get();

        foreach ($items as $item) {
            $weight = $item->weight - 1;
            $item->update(['weight' => $weight]);
        }

        return;
    }
}
