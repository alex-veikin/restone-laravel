<?php

namespace App\Http\Controllers;

use App\AdminNavLink;
use Illuminate\Http\Request;

class AdminNavLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminNavLink  $adminNavLink
     * @return \Illuminate\Http\Response
     */
    public function show(AdminNavLink $adminNavLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminNavLink  $adminNavLink
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminNavLink $adminNavLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminNavLink  $adminNavLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminNavLink $adminNavLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminNavLink  $adminNavLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminNavLink $adminNavLink)
    {
        //
    }
}
