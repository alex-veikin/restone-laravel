<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;
use App\Page;
use App\Cuisine;
use App\Category;
use App\Cook;
use App\User;
use Auth;

class PageController extends Controller
{
    /**
     * Вывод списка всех страниц в админке
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin()
    {
        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'cuisines' => Cuisine::all(),
            'categories'     => Category::all(),
            'cooks'    => Cook::all()->sortBy('weight'),
        ];

        return view('site.index', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageRequest $request)
    {
        Page::create($request->all());

        return redirect()->route('admin.pages.index')
                         ->with("success", "Запись успешно создана!");
    }

    /**
     * @param $alias
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($alias)
    {
        $page = Page::where('alias', $alias)->first();

        if (!$page) {
            return abort(404);
        }

        return view('site.page', compact('page'));
    }

    /**
     * @param \App\Page $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * @param \App\Http\Requests\PageRequest $request
     * @param \App\Page                      $page
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageRequest $request, Page $page)
    {
        $page->update($request->all());

        return redirect()->back()->with("success", "Запись успешно изменена!");
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Page                $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function destroy(Request $request, Page $page)
    {
        if ($request->isMethod('delete')) {
            $page->delete();

            return redirect()->route('admin.pages.index')
                             ->with("success", "Запись удалена!");
        }

        return view('admin.pages.delete', compact('page'));
    }
}
