<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Page;
use App\TopNav;
use App\SideNav;
use App\Cuisine;
use App\Category;
use App\Dish;
use App\Cook;
use App\Order;
use Auth;

class AdminController extends Controller
{

    /**
     * Главная страница админки
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users      = User::all();
        $pages      = Page::all();
        $cuisines   = Cuisine::all();
        $categories = Category::all();
        $dishes     = Dish::all();
        $cooks      = Cook::all();
        $orders     = Order::all();
        $top_nav    = TopNav::all();
        $side_nav   = SideNav::all();

        $data = [
            'users'      => $users,
            'pages'      => $pages,
            'cuisines'   => $cuisines,
            'categories' => $categories,
            'dishes'     => $dishes,
            'cooks'      => $cooks,
            'orders'     => $orders,
            'top_nav'    => $top_nav,
            'side_nav'   => $side_nav,
        ];

        return view('admin.index', $data);
    }
}
