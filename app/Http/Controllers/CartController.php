<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Auth;
use App\Dish;

class CartController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('site.cart.index');
    }

    public function ajaxIndex()
    {
        return view('site.cart.ajaxIndex');
    }

    public function apiIndex()
    {
        $cart = new Cart();
        $data = [
            'cart' => [
                'items' => $cart->items,
                'total_qty' => $cart->totalQty,
                'total_price' => $cart->totalPrice
            ]
        ];
        return session()->has('cart') ? $data : null;
    }


    /**
     * @param \App\Dish $dish
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function increaseItem(Dish $dish)
    {
        // Создаем объект с данными корзины
        $cart = new Cart();

        // Добавляем продукт в корзину
        $cart->increase($dish);

        // Записываем корзину в сессию
        session(['cart' => $cart]);

        return redirect()->back();
    }

    public function apiIncreaseItem($id)
    {
        $dish = Dish::find($id);
        $cart = new Cart();
        $cart->increase($dish);
        session(['cart' => $cart]);
        $data = [
            'cart' => [
                'items' => $cart->items,
                'total_qty' => $cart->totalQty,
                'total_price' => $cart->totalPrice
            ]
        ];
        return $data;
    }


    /**
     * @param \App\Dish $dish
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function decreaseItem(Dish $dish)
    {
        // Создаем объект с данными корзины
        $cart = new Cart();

        // Уменьшаем количество
        $cart->decrease($dish);

        // Если корзина не пустая
        if (session()->has('cart')) {
            // Записываем корзину в сессию
            session(['cart' => $cart]);
        }

        return redirect()->back();
    }

    public function apiDecreaseItem($id)
    {
        $dish = Dish::find($id);
        $cart = new Cart();
        $cart->decrease($dish);
        if (session()->has('cart')) {
            session(['cart' => $cart]);

            $data = [
                'cart' => [
                    'items' => $cart->items,
                    'total_qty' => $cart->totalQty,
                    'total_price' => $cart->totalPrice
                ]
            ];
            return $data;
        }

        return null;
    }


    /**
     * @param \App\Dish $dish
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeItem(Dish $dish)
    {
        // Создаем объект с данными корзины
        $cart = new Cart();

        // Удаляем пункт
        $cart->remove($dish);

        // Если корзина не пустая
        if (session()->has('cart')) {
            // Записываем корзину в сессию
            session(['cart' => $cart]);
        }

        return redirect()->back();
    }

    public function apiRemoveItem($id)
    {
        $dish = Dish::find($id);
        $cart = new Cart();
        $cart->remove($dish);
        if (session()->has('cart')) {
            session(['cart' => $cart]);

            $data = [
                'cart' => [
                    'items' => $cart->items,
                    'total_qty' => $cart->totalQty,
                    'total_price' => $cart->totalPrice
                ]
            ];
            return $data;
        }

        return null;
    }


    /**
     * Чистим корзину
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearCart()
    {
        Cart::clear();

        return redirect()->back();
    }

    public function apiClearCart()
    {
        Cart::clear();

        return $this->apiIndex();
    }
}
