<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'weight'      => 'required|numeric',
                'alias'       => 'required|alpha_dash|unique:cooks,alias,'
                                 . $this->cook->id,
                'full_name'   => 'required|string|max:255|unique:cooks,full_name,'
                                 . $this->cook->id,
                'title'       => 'required|string|max:255|unique:cooks,title,'
                                 . $this->cook->id,
                'description' => 'required|string',
                'file'       => [
                    'image',
                    'mimes:jpeg,png,jpg,gif,svg',
                    'max:2048',
                    function ($attribute, $value, $fail) {
                        if (file_exists(public_path() . '/img/cooks/'
                                        . $value->getClientOriginalName())
                        ) {
                            return $fail('Файл с таким именем уже существует.');
                        }
                    },
                ],
            ];
        } else {
            return [
                'weight'      => 'required|numeric',
                'alias'       => 'required|alpha_dash|unique:cooks,alias',
                'full_name'   => 'required|string|max:255|unique:cooks,full_name',
                'title'       => 'required|string|max:255|unique:cooks,title',
                'description' => 'required|string',
                'file'       => [
                    'image',
                    'mimes:jpeg,png,jpg,gif,svg',
                    'max:2048',
                    function ($attribute, $value, $fail) {
                        if (file_exists(public_path() . '/img/cooks/'
                                        . $value->getClientOriginalName())
                        ) {
                            return $fail('Файл с таким именем уже существует.');
                        }
                    },
                ],
            ];
        }
    }
}
