<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'alias'   => 'required|alpha_dash|unique:pages,alias,'
                             . $this->page->id,
                'title'   => 'required|string|max:255',
                'content' => 'string|nullable',
            ];
        } else {
            return [
                'alias'   => 'required|alpha_dash|unique:pages,alias',
                'title'   => 'required|string|max:255',
                'content' => 'string|nullable',
            ];
        }
    }
}
