<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CuisineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'alias'       => 'required|alpha_dash|unique:cuisines,alias,'
                                 . $this->cuisine->id,
                'title'       => 'required|string|max:255|unique:cuisines,title,'
                                 . $this->cuisine->id,
                'description' => 'required|string',
                'file'       => [
                    'image',
                    'mimes:jpeg,png,jpg,gif,svg',
                    'max:2048',
                    function ($attribute, $value, $fail) {
                        if (file_exists(public_path() . '/img/cuisines/'
                                        . $value->getClientOriginalName())
                        ) {
                            return $fail('Файл с таким именем уже существует.');
                        }
                    },
                ],
            ];
        } else {
            return [
                'alias'       => 'required|alpha_dash|unique:cuisines,alias',
                'title'       => 'required|string|max:255|unique:cuisines,title',
                'description' => 'required|string',
                'file'       => [
                    'image',
                    'mimes:jpeg,png,jpg,gif,svg',
                    'max:2048',
                    function ($attribute, $value, $fail) {
                        if (file_exists(public_path() . '/img/cuisines/'
                                        . $value->getClientOriginalName())
                        ) {
                            return $fail('Файл с таким именем уже существует.');
                        }
                    },
                ],
            ];
        }
    }
}
