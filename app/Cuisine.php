<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{

    protected $fillable = ['alias', 'title', 'description', 'image'];

    public function dishes()
    {
        return $this->hasMany('App\Dish');
    }
}
