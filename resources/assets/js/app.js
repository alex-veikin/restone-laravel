require('./bootstrap');

window.Vue = require('vue');
window.VueRouter = require('vue-router');

// Vue.use(VueRouter);

import Cart from './components/Cart'

// const router = new VueRouter({
//     mode: 'history',
//     routes: [
//         {
//             path: '/cart',
//             name: 'cart',
//             component: Cart
//         },
//     ]
// });

// Vue.component('cart', require('./components/cart.vue'));

const cart = new Vue({
    el: '#app',
    components: { Cart },
    // router
});


$(function () {

//  Authorization form
//////////////////////
    $('.login-open').click(function () { //Show-hide authorization
        if ($('.login-open').hasClass("login-open--show")) {
            $('.login-open').removeClass("login-open--show");
            $('.header__top-auth').slideUp(400);
        } else {
            $('.login-open').addClass("login-open--show");
            $('.header__top-auth').slideDown(400).css({
                'display': 'flex'
            })
        }
    });

    $(window).resize(function () { //Hide auth when resize window
        if ($('.login-open').hasClass("login-open--show")) {
            $('.login-open').removeClass("login-open--show");
        } else {
            $('.header__top-auth').removeAttr("style");
        }
    });

    $('body').click(function (event) { //Hide auth when click outside header__top
        if ($('.login-open').hasClass("login-open--show")) {
            if (!$(event.target).closest('.header__top').length) {
                $('.login-open').removeClass("login-open--show");
                $('.header__top-auth').slideUp(400);
            }
        }
    });


//  Margin for .index
/////////////////////
    function marginIndex() {
        var margin = $(".header").height() - $(".header__top").height();
        $(".index").css({
            "margin-top": ((margin) * -1),
            "min-height": ($(window).height() - $(".header__top").height())
        });
    }

    marginIndex();

    $(window).resize(marginIndex);


//Slider OwlCarousel
////////////////////
    $(".owl-carousel").owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        loop: true,
        autoplayHoverPause: true,
        smartSpeed: 500,
        margin: 15,
        lazyLoad: true,
        mouseDrag: true,
        touchDrag: true,
        navText: [
            "<i class=\"fas fa-angle-double-left\"></i>",
            "<i class=\"fas fa-angle-double-right\"></i>"
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                center: true,
                // autoWidth:true
            },
            560: {
                items: 2
                // center:true,
                // autoWidth:true
                // loop:true
            },
            780: {
                items: 3
                // loop:true,
                // nav:true
            },
            960: {
                items: 4
                // loop:true
                // nav:true
            }
        }
    });


// Hide all submenus when window loaded
    $('.submenu-btn ~ ul').hide();

// Show/hide submenu
    $('.submenu-btn').on('click', function () {
        $(this).siblings('ul').slideToggle();
    });

// Hide auth when click outside submenu
    $('body').click(function (e) {
        if ($('.submenu-btn ~ ul').is(':visible')) {
            if (!$(e.target).closest('.submenu-btn').length) {
                $('.submenu-btn ~ ul').slideUp();
            }
        }
    });


// Show side menu
    $('.nav-open-btn').on('click', function () {
        $(this).siblings('.side-nav').addClass('show');
        $(this).siblings('.nav-close-btn').addClass('show');
    });

// Hide side menu
    $('.nav-close-btn').on('click', function () {
        $(this).siblings('.side-nav').removeClass('show');
        $(this).removeClass('show');
    });


});
