<ul>
    @foreach ($current as $link)
        <li>
            @if($link->type == 'url')
                <a href='{{ url($link->url) }}'>{{ $link->url_title }}</a>
            @elseif($link->type == 'page')
                @if( Request::path() == $link->page->alias )
                    <a class='link-active' href='{{ url($link->page->alias) }}'>{{ $link->page->title }}</a>
                @else
                    <a href='{{ url($link->page->alias) }}'>{{ $link->page->title }}</a>
                @endif
            @elseif($link->type == 'submenu')
                <button class="submenu-btn">
                    {{ $link->submenu_title }}
                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                </button>

                {!! view('layouts.side-menu', ['nav' => $nav, 'current' => $nav->where('parent_id', $link->id)->all()]) !!}
            @endif
        </li>
    @endforeach
</ul>