@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', $category->title . ' | ')

@section('content')
    <section class="cuisine">
        <div class="wrapper">
            <h2 class="h2">{{ $category->title }}</h2>

            <div class="description">{{ $category->description }}</div>

            @include('layouts.dishes')
        </div>
    </section>
@endsection