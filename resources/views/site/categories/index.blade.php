@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Категории блюд | ')

@section('content')
    <section class="cuisine">
        <div class="wrapper">
            <h2 class="h2">Категории блюд</h2>

            <div class="cuisine__items cuisine__items--flex">
                @foreach ( $categories as $category )
                    <div class="item item--flex">
                        <a href="{{ url('categories/'.$category->alias) }}">
                            <div class="image">
                                <div class="overlay">
                                    <p>Перейти в раздел</p>
                                </div>
                                <img src="{{ asset('img/categories/' . $category->image) }}"
                                     alt="{{ $category->title }}">
                            </div>
                            <div class="title side-lines">{{ $category->title }}</div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection