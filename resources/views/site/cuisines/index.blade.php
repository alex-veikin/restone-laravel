@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Национальные кухни | ')

@section('content')
    <section class="cuisine">
        <div class="wrapper">
            <h2 class="h2">Национальные кухни</h2>

            <div class="cuisine__items cuisine__items--flex">
                @foreach ( $cuisines as $cuisine )
                    <div class="item item--flex">
                        <a href="{{ url('cuisines/'.$cuisine->alias) }}">
                            <div class="image">
                                <div class="overlay">
                                    <p>Перейти в раздел</p>
                                </div>
                                <img src="{{ asset('img/cuisines/' . $cuisine->image) }}"
                                     alt="{{ $cuisine->title }} кухня">
                            </div>
                            <div class="title side-lines">{{ $cuisine->title }}</div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection