@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Корзина | ')

@section('content')
    <section class="cart">
        <div class="wrapper">
            <cart></cart>
            {{--@if(session('cart'))--}}
                {{--<table>--}}
                {{--<tr>--}}
                {{--<th>№</th>--}}
                {{--<th>Блюдо</th>--}}
                {{--<th>Цена</th>--}}
                {{--<th>Кол-во</th>--}}
                {{--<th>Сумма</th>--}}
                {{--<th>Удалить</th>--}}
                {{--</tr>--}}
                {{--@foreach(session('cart')->items as $dish)--}}
                {{--<tr>--}}
                {{--<td>{{ $loop->iteration }}</td>--}}
                {{--<td>{{ $dish['item']->title }}</td>--}}
                {{--<td>{{ $dish['item']->price }} руб.</td>--}}
                {{--<td>--}}
                {{--<div class="qty">--}}
                {{--<a href="{{ route('cart.decrease', $dish['item']) }}">-</a>--}}
                {{--<span>{{ $dish['qty'] }}</span>--}}
                {{--<a href="{{ route('cart.increase', $dish['item']) }}">+</a>--}}
                {{--</div>--}}
                {{--</td>--}}
                {{--<td>{{ $dish['price'] }} руб.</td>--}}
                {{--<td>--}}
                {{--<div class="delete">--}}
                {{--<a href="{{ route('cart.remove', $dish['item']) }}">--}}
                {{--<i class="fa fa-trash" aria-hidden="true"></i>--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--@endforeach--}}
                {{--<tr class="total">--}}
                {{--<td colspan="3">Итого:</td>--}}
                {{--<td>{{ session('cart')->totalQty }}</td>--}}
                {{--<td colspan="2">{{ session('cart')->totalPrice }} руб.</td>--}}
                {{--</tr>--}}
                {{--</table>--}}

                {{--<div class="manage">--}}
                    {{--{{ link_to_route('cart.clear', 'Очистить корзину', null, ['class' => 'clear']) }}--}}
                    {{--{{ link_to_route('order.store', 'Оформить заказ', null,--}}
                    {{--['class' => 'order', 'onclick' => 'event.preventDefault(); document.getElementById("order-store").submit();']) }}--}}
                    {{--{!! Form::open(['route'=>'order.store', 'id'=>'order-store', 'style'=>'display:none']) !!}--}}
                    {{--{!! Form::close() !!}--}}
                {{--</div>--}}
            {{--@else--}}
                {{--<p>В вашей корзине нет товаров</p>--}}
            {{--@endif--}}
        </div>
    </section>
@endsection