@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', $dish->title . ' | ')

@section('content')
    <section class="dish">
        <div class="wrapper">
            {{--{{ link_to_route('dishes.index', 'Все блюда') }}--}}

            <div class="dish__show">
                <div class="image">
                    <img src="{{ asset('img/dishes/' . $dish->image) }}" alt="{{ $dish->title }}">
                </div>

                <div class="info">
                    <div class="title">{{ $dish->title }}</div>

                    <div class="links">
                        {{ link_to_route('cuisines.show', $dish->cuisine->title.' кухня', $dish->cuisine->alias) }}
                        {{ link_to_route('categories.show', $dish->category->title, $dish->category->alias) }}
                    </div>

                    <div class="description">{!! $dish->description !!}</div>
                    <div class="bottom">
                        <div class="price">Цена: {{ $dish->price }} руб.</div>
                        <a href="{{ route('cart.increase', $dish) }}" class="buy">В корзину</a>
                    </div>
                </div>
            </div>

            <div class="similar">
                <div class="title">Похожие блюда</div>

                <div class="owl-carousel owl-theme owl-loaded owl-drag similar__items">
                    @foreach ( $similar as $dish )
                        <div class="item">
                            <div class="image">
                                <div class="links">
                                    {{ link_to_route('cuisines.show', $dish->cuisine->title.' кухня', $dish->cuisine->alias) }}
                                    {{ link_to_route('categories.show', $dish->category->title, $dish->category->alias) }}
                                </div>

                                <a href="{{ route('dishes.show', [$dish]) }}">
                                    <div class="overlay">
                                        <p>Посмотреть описание</p>
                                    </div>

                                    <div class="title">{{ $dish->title }}</div>

                                    <img src="{{ asset('img/dishes/' . $dish->image) }}" alt="{{ $dish->title }}">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection