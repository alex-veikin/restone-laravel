@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Все блюда | ')

@section('content')
    <section class="dish">
        <div class="wrapper">
            <h2 class="h2">Все блюда</h2>

            @include('layouts.dishes')
        </div>
    </section>
@endsection