@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Главная | ')

@section('content')
    <section class="index">
        <div class="wrapper">
            <div class="index__welcome">
                <span class="index__welcome-1 side-lines">Добро пожаловать в</span>
                <span class="index__welcome-2">Ресторан</span>
                <span class="index__welcome-3">лучших блюд мировой кухни</span>
                <span class="index__welcome-4">с доставкой на дом</span>
            </div>
        </div>
    </section>

    <section class="cuisine">
        <div class="wrapper">
            <h2 class="h2">Кухни стран мира</h2>

            <div class="owl-carousel owl-theme owl-loaded owl-drag cuisine__items">
                @foreach ( $cuisines as $cuisine )
                    <div class="item">
                        <a href="{{ url('cuisines/'.$cuisine->alias) }}">
                            <div class="image">
                                <div class="overlay">
                                    <p>Перейти в раздел</p>
                                </div>
                                <img src="{{ asset('img/cuisines/' . $cuisine->image) }}"
                                     alt="{{ $cuisine->title }} кухня">
                            </div>
                            <div class="title side-lines">{{ $cuisine->title }}</div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="section__description">
                <div class="section__description-icon">
                    <img src="{{ asset('img/icon-world.png') }}" alt="icon">
                </div>
                <div class="section__description-text">
                    В ресторане <span>Rest </span><span class="one">One</span> Вы всегда сможете заказать вкуснейшие
                    блюда
                    мировой кухни. Вам будет предложена качественная и вкусная кухня, в которой важное место уделяется
                    изысканности блюд и разнообразию вкусов.
                </div>
            </div>
        </div>
    </section>

    <section class="category">
        <div class="wrapper">
            <h2 class="h2">Меню ресторана</h2>

            <div class="owl-carousel owl-theme owl-loaded owl-drag category__items">
                @foreach ( $categories as $category )
                    <div class="item">
                        <a href="{{ url('categories/'.$category->alias) }}">
                            <div class="image">
                                <div class="overlay">
                                    <p>Перейти в раздел</p>
                                </div>
                                <img src="{{ asset('img/categories/' . $category->image) }}" alt="{{ $category->title }}">
                            </div>
                            <div class="title side-lines">{{ $category->title }}</div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="section__description">
                <div class="section__description-icon">
                    <img src="{{ asset('img/icon-menu.png') }}" alt="icon">
                </div>
                <div class="section__description-text">
                    Меню ресторана <span>Rest </span><span class="one">One</span> включает самые популярные блюда разных
                    стран мира, а так же оригинальные творения от нашего шеф-повара. В более 20 разделах нашего меню Вы
                    найдете бдюда на любой вкус.
                </div>
            </div>
        </div>
    </section>

    <section class="cooks">
        <div class="wrapper">
            <h2 class="h2">Наши повара</h2>

            <div class="cooks__items">
                @foreach ( $cooks as $cook )
                    <div class="{{ $cook->title == 'шеф-повар' ? 'cooks__item cooks__item--big' : 'cooks__item' }}">
                        <div class="cooks__item-img">
                            <img src="{{ asset('img/cooks/' . $cook->image) }}" alt="{{ $cook->full_name }}">
                        </div>
                        <div class="cooks__item-title">{{ $cook->full_name }}<span>{{ $cook->title }}</span></div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection