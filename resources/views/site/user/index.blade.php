@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Кабинет пользователя | ')

@section('content')
    <section>
        <div class="wrapper">
            <h2 class="h2">Кабинет пользователя</h2>

            <div class="user-content">
                <div class="item">
                    <div class="info">
                        <div class="info-title">Мои данные</div>
                        <p class="info-row"><span>Имя: </span>{{ $user->first_name }}</p>
                        <p class="info-row"><span>Фамилия: </span>{{ $user->last_name }}</p>
                        <p class="info-row"><span>Логин: </span>{{ $user->login }}</p>
                        <p class="info-row"><span>E-mail: </span>{{ $user->email }}</p>
                        <p class="info-row"><span>Дата регистрации: </span>{{ $user->created_at }}</p>
                        <button class="info-btn">
                            {{ link_to_route('user.edit', 'Изменить данные') }}
                        </button>
                    </div>
                </div>

                <div class="item">
                    <div class="info">
                        <div class="info-title">Мои заказы</div>
                        @if($orders->count())
                            @foreach($orders as $order)
                                <p class="info-order">
                                    <span>Заказ #{{ $order->id }}:</span>
                                    <span>товаров: {{ unserialize($order->cart)->totalQty }},</span>
                                    <span>сумма: {{ unserialize($order->cart)->totalPrice }} рублей</span>
                                    <a href="{{ route('order.show', $order) }}">Подробнее</a>
                                </p>
                            @endforeach
                        @else
                            <p class="info-order">У вас нет активных заказов</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection