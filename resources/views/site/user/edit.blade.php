@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Кабинет пользователя | ')

@section('content')
    <section>
        <div class="wrapper">
            <h2 class="h2">Редактировать данные</h2>

            <div class="user-content">
                @if(session()->has('success'))
                    <p class="success">{{ session('success') }}</p>
                @endif

                <div class="item">
                    {!! Form::open(['route'=>'user.update.profile']) !!}
                    {!! Form::text('first_name', $user->first_name, ['placeholder'=>'имя']) !!}
                    @if($errors->has('first_name'))
                        <p class="error">{{ $errors->first('first_name') }}</p>
                    @endif
                    {!! Form::text('last_name', $user->last_name, ['placeholder'=>'фамилия']) !!}
                    @if($errors->has('last_name'))
                        <p class="error">{{ $errors->first('last_name') }}</p>
                    @endif
                    {!! Form::password('password', ['placeholder'=>'текущий пароль']) !!}
                    @if($errors->has('password'))
                        <p class="error">{{ $errors->first('password') }}</p>
                    @endif
                    {!! Form::submit('Изменить данные', ['class'=>'submit']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="item">
                    {!! Form::open(['route'=>'user.update.password']) !!}
                    {!! Form::password('password_old', ['placeholder'=>'текущий пароль']) !!}
                    @if($errors->has('password_old'))
                        <p class="error">{{ $errors->first('password_old') }}</p>
                    @endif
                    {!! Form::password('password_new', ['placeholder'=>'новый пароль']) !!}
                    @if($errors->has('password_new'))
                        <p class="error">{{ $errors->first('password_new') }}</p>
                    @endif
                    {!! Form::password('password_new_confirmation', ['placeholder'=>'подтвердите пароль']) !!}
                    {!! Form::submit('Изменить пароль', ['class'=>'submit']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="item">
                    {{ link_to_route('user.delete', 'Удалить аккаунт') }}
                    {{ link_to_route('user.index', 'Назад') }}
                </div>
            </div>
        </div>
    </section>
@endsection