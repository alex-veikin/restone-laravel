@extends('layouts.site')

@section('pageDescription', '')
@section('pageTitle', 'Кабинет пользователя | ')

@section('content')
    <section>
        <div class="wrapper">
            <h2 class="h2">Заказ #{{ $order->id }}</h2>

            <div class="cart">
                <table>
                    <tr>
                        <th>№</th>
                        <th>Блюдо</th>
                        <th>Цена</th>
                        <th>Кол-во</th>
                        <th>Сумма</th>
                    </tr>
                    @foreach($cart->items as $dish)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $dish['item']->title }}</td>
                            <td>{{ $dish['item']->price }} руб.</td>
                            <td>{{ $dish['qty'] }}</td>
                            <td>{{ $dish['price'] }} руб.</td>
                        </tr>
                    @endforeach
                    <tr class="total">
                        <td colspan="3">Итого:</td>
                        <td>{{ $cart->totalQty }}</td>
                        <td>{{ $cart->totalPrice }} руб.</td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
@endsection