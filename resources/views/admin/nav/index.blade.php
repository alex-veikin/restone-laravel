@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $nav))

@section('new-item')
    <a href="#" class="add add-item-btn">Новый пункт</a>
@endsection

@section('content')
    @if($errors->has('url_title'))
        <p class="error">{{ $errors->first('url_title') }}</p>
    @endif
    @if($errors->has('url'))
        <p class="error">{{ $errors->first('url') }}</p>
    @endif
    @if($errors->has('page_id'))
        <p class="error">{{ $errors->first('page_id') }}</p>
    @endif
    @if($errors->has('submenu_title'))
        <p class="error">{{ $errors->first('submenu_title') }}</p>
    @endif
    @if($errors->has('parent_id'))
        <p class="error">{{ $errors->first('parent_id') }}</p>
    @endif

    {!! view('admin.nav.menu_list', ['nav' => $nav, 'nav_menu' => $nav_menu,'current' => $nav_menu->where('parent_id', 0)]) !!}

    <div class="overlay">
        <div class="add-nav-item">
            <button class="close-btn">
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>

            <div class="buttons">
                <button class="active">
                    <label for="url">Ссылка</label>
                </button>
                <button>
                    <label for="page">Страница</label>
                </button>
                <button>
                    <label for="submenu">Подменю</label>
                </button>
            </div>

            {!! Form::open(['route'=>['admin.nav.store', $nav]]) !!}
            {!! Form::radio('type', 'url', true, ['id'=>'url', 'hidden']) !!}
            {!! Form::radio('type', 'page', null, ['id'=>'page', 'hidden']) !!}
            {!! Form::radio('type', 'submenu', null, ['id'=>'submenu', 'hidden']) !!}
            <div class="group url active">
                {!! Form::label('Имя пункта') !!}
                {!! Form::text('url_title', null, ['placeholder'=>'имя пункта']) !!}
            </div>
            <div class="group url active">
                {!! Form::label('Ссылка') !!}
                {!! Form::text('url', 'http://', ['placeholder'=>'ссылка (http://...)']) !!}
            </div>
            <div class="group page">
                {!! Form::label('Страница') !!}
                {!! Form::select('page_id', $pages_select, null) !!}
            </div>
            <div class="group submenu">
                {!! Form::label('Подменю') !!}
                {!! Form::text('submenu_title', null, ['placeholder'=>'имя пункта']) !!}
            </div>
            <div class="group">
                {!! Form::label('Родительский пункт') !!}
                {!! Form::select('parent_id', $parents_select, null) !!}
            </div>
            <div class="group">
                {!! Form::submit('Добавить пункт меню', ['class' => 'submit']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection