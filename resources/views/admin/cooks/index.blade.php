@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.cooks.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th>Номер</th>
            <th>Имя</th>
            <th>Должность</th>
        </tr>
        @foreach($cooks as $cook)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.cooks.edit', $cook->weight, [$cook]) }}
                </td>
                <td>{{ $cook->full_name }}</td>
                <td>{{ $cook->title }}</td>
            </tr>
        @endforeach
    </table>
@endsection