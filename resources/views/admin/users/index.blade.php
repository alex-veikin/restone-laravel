@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.users.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th class="small">ID</th>
            <th>Права</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Логин</th>
            <th>Email</th>
            <th> Дата регистрация</th>
        </tr>
        @foreach($users as $user)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.users.edit', $user->id, [$user]) }}
                </td>
                <td>{{ $user->role }}</td>
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->last_name }}</td>
                <td>{{ $user->login }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->created_at }}</td>
            </tr>
        @endforeach
    </table>
@endsection