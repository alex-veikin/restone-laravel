@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    {!! Form::open(['route'=>'admin.users.store']) !!}
    <div class="group">
        {!! Form::label('Права') !!}
        {!! Form::select('role', ['user'=>'user', 'admin'=>'admin'], old('role')) !!}
        @if($errors->has('role'))
            <p class="error">{{ $errors->first('role') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Имя') !!}
        {!! Form::text('first_name', old('first_name'), ['placeholder'=>'имя']) !!}
        @if($errors->has('first_name'))
            <p class="error">{{ $errors->first('first_name') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Фамилия') !!}
        {!! Form::text('last_name', old('last_name'), ['placeholder'=>'Фамилия']) !!}
        @if($errors->has('last_name'))
            <p class="error">{{ $errors->first('last_name') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Логин') !!}
        {!! Form::text('login', old('login'), ['placeholder'=>'Логин']) !!}
        @if($errors->has('login'))
            <p class="error">{{ $errors->first('login') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Email') !!}
        {!! Form::email('email', old('email'), ['placeholder'=>'Email']) !!}
        @if($errors->has('email'))
            <p class="error">{{ $errors->first('email') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Пароль') !!}
        {!! Form::password('password', ['placeholder'=>'Пароль']) !!}
        @if($errors->has('password'))
            <p class="error">{{ $errors->first('password') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Подтвердите пароль') !!}
        {!! Form::password('password_confirmation', ['placeholder'=>'Подтвердите пароль']) !!}
    </div>
    <div class="group">
        {!! Form::submit('Сохранить', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection