@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $user))

@section('content')
    {!! Form::open(['route'=>['admin.users.destroy', $user], 'method'=>'delete']) !!}
    <div class="group">
        {!! Form::submit('Удалить пользователя', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection