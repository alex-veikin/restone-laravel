@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $cuisine))

@section('content')
    {!! Form::open(['route'=>['admin.cuisines.destroy', $cuisine], 'method'=>'DELETE']) !!}
    <div class="group">
        {!! Form::submit('Удалить пункт', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection