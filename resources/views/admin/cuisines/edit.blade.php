@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $cuisine))

@section('new-item', link_to_route('admin.cuisines.delete', 'Удалить запись',[$cuisine] , ['class'=>'delete']))

@section('content')
    {!! Form::model($cuisine, [
                        'route'=>['admin.cuisines.update', $cuisine],
                        'enctype'=>'multipart/form-data',
                        'method'=>'PATCH'
                    ]) !!}
    <div class="group">
        {!! Form::label('Alias') !!}
        {!! Form::text('alias', null, ['placeholder'=>'Псевдоним']) !!}
        @if($errors->has('alias'))
            <p class="error">{{ $errors->first('alias') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Заголовок') !!}
        {!! Form::text('title', null, ['placeholder'=>'Заголовок']) !!}
        @if($errors->has('title'))
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::label('Описание') !!}
        {!! Form::textarea('description', null, ['placeholder'=>'Описание', 'id'=>'ckeditor']) !!}
        @if($errors->has('description'))
            <p class="error">{{ $errors->first('description') }}</p>
        @endif
    </div>
    <div class="group">
        <img src="{{ asset('/img/cuisines/'.$cuisine->image) }}" alt="{{ $cuisine->title }} кухня">
    </div>
    <div class="group">
        {!! Form::label('Изменить изображение') !!}
        {!! Form::file('file') !!}
        @if($errors->has('file'))
            <p class="error">{{ $errors->first('file') }}</p>
        @endif
    </div>
    <div class="group">
        {!! Form::submit('Сохранить изменения', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection