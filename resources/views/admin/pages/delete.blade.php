@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $page))

@section('content')
    {!! Form::open(['route'=>['admin.pages.destroy', $page], 'method'=>'delete']) !!}
    <div class="group">
        {!! Form::submit('Удалить страницу', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection