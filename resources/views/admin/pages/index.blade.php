@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.pages.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>Alias</th>
            <th>Заголовок</th>
        </tr>
        @foreach($pages as $page)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.pages.edit', $page->id, [$page]) }}
                </td>
                <td>{{ $page->alias }}</td>
                <td>{{ $page->title }}</td>
            </tr>
        @endforeach
    </table>
@endsection