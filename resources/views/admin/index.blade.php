@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('content')
    <div class="info-items">
        <div class="info-item">
            <a href="{{ route('admin.users.index') }}">
                <div class="title">Пользователи</div>
                <div class="info">{{ $users->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.pages.index') }}">
                <div class="title">Страницы</div>
                <div class="info">{{ $pages->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.cuisines.index') }}">
                <div class="title">Кухни</div>
                <div class="info">{{ $cuisines->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.categories.index') }}">
                <div class="title">Категории</div>
                <div class="info">{{ $categories->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.dishes.index') }}">
                <div class="title">Блюда</div>
                <div class="info">{{ $dishes->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.cooks.index') }}">
                <div class="title">Повара</div>
                <div class="info">{{ $cooks->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.orders.index') }}">
                <div class="title">Заказы</div>
                <div class="info">
                    <span class="active">{{ $orders->where('status',1)->count() }}</span>
                    <span>/</span>
                    <span class="inactive">{{ $orders->where('status',0)->count() }}</span>
                </div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.nav.index', ['top']) }}">
                <div class="title">Верхнее меню</div>
                <div class="info">{{ $top_nav->count() }}</div>
            </a>
        </div>
        <div class="info-item">
            <a href="{{ route('admin.nav.index', ['side']) }}">
                <div class="title">Боковое меню</div>
                <div class="info">{{ $side_nav->count() }}</div>
            </a>
        </div>
    </div>
@endsection