@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName(), $category))

@section('content')
    {!! Form::open(['route'=>['admin.categories.destroy', $category], 'method'=>'DELETE']) !!}
    <div class="group">
        {!! Form::submit('Удалить пункт', ['class' => 'submit']) !!}
    </div>
    {!! Form::close() !!}
@endsection