@extends('layouts.admin')

@section('pageDescription', '')
@section('pageTitle', 'Админпанель | ')

@section('breadcrumb', Breadcrumbs::render(Route::currentRouteName()))

@section('new-item', link_to_route('admin.dishes.create', 'Новая запись',[] , ['class'=>'add']))

@section('content')
    <table>
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Кухня</th>
            <th>Категория</th>
        </tr>
        @foreach($dishes as $dish)
            <tr>
                <td class="small">
                    {{ link_to_route('admin.dishes.edit', $dish->id, [$dish]) }}
                </td>
                <td>{{ $dish->title }}</td>
                <td>{{ $dish->cuisine->title }}</td>
                <td>{{ $dish->category->title }}</td>
            </tr>
        @endforeach
    </table>
@endsection