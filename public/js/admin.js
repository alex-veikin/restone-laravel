/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 45);
/******/ })
/************************************************************************/
/******/ ({

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(46);


/***/ }),

/***/ 46:
/***/ (function(module, exports) {

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// $(function () {

var admin_sidebar = $('.admin_sidebar'),
    admin_sidebar__button = $('.admin_sidebar__button');

// Admin sidebar open-close
admin_sidebar__button.on('click', function () {
    admin_sidebar.toggleClass('show');

    if (admin_sidebar.hasClass('show')) {
        admin_sidebar__button.find('.open').hide();
        admin_sidebar__button.find('.close').show();
    } else {
        admin_sidebar__button.find('.open').show();
        admin_sidebar__button.find('.close').hide();
    }
});

var btn = $('.add-nav-item .buttons button');
var group_type = $('form .group');

// Choose type of the nav link
btn.on('click', function () {
    // get selected type
    var type = $(this).find('label').attr('for');

    // add style for selected button
    $(this).addClass('active');
    btn.not(this).removeClass('active');

    // show form of selected type
    group_type.each(function () {
        if ($(this).hasClass(type)) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
});

// Hide the record entry form on load window
$('.overlay').addClass('hide');

// Show form
$('.add-item-btn').on('click', function () {
    $('.overlay').removeClass('hide').addClass('show');
});

// Hide form
$('.close-btn').on('click', function () {
    $('.overlay').removeClass('show').addClass('hide');
});

// confirm to delete menu item
$('.delete-item-btn').on('click', function (e) {
    if (!confirm('Удалить пункт меню?')) {
        e.preventDefault();
    }
});

CKEDITOR.replace('ckeditor');

// });

/***/ })

/******/ });