<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            ['title' => 'Главная', 'alias' => '/', 'content' => ''],
            ['title' => 'Кухни', 'alias' => 'cuisines', 'content' => ''],
            ['title' => 'Категории', 'alias' => 'categories', 'content' => ''],
            ['title' => 'Все блюда', 'alias' => 'dishes', 'content' => ''],
            ['title' => 'Повара', 'alias' => 'cooks', 'content' => ''],
            ['title' => 'Контакты', 'alias' => 'contacts', 'content' => ''],
            ['title' => 'Доставка', 'alias' => 'shipping', 'content' => ''],
            ['title' => 'О нас', 'alias' => 'about', 'content' => ''],
            ['title' => 'Отзывы', 'alias' => 'reviews', 'content' => ''],
            ['title' => 'Корзина', 'alias' => 'cart', 'content' => ''],
            ['title' => 'Галерея', 'alias' => 'gallery', 'content' => ''],
            ['title' => 'Статьи', 'alias' => 'articles', 'content' => ''],
        ]);
    }
}
