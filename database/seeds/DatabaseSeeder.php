<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            PagesTableSeeder::class,
            TopNavsTableSeeder::class,
            SideNavsTableSeeder::class,
            CuisinesTableSeeder::class,
            CategoriesTableSeeder::class,
            CooksTableSeeder::class,
            DishesTableSeeder::class,
            AdminNavLinksTableSeeder::class,
        ]);
    }
}
