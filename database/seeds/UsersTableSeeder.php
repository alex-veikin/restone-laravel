<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
              'role' => 'user',
              'first_name' => 'User',
              'last_name' => 'One',
              'login' => 'user-one',
              'email' => 'userone@tut.by',
              'password' => bcrypt('123456')
            ],
            [
              'role' => 'admin',
              'first_name' => 'Admin',
              'last_name' => 'One',
              'login' => 'admin-one',
              'email' => 'admonone@tut.by',
              'password' => bcrypt('456789')
            ],
        ]);
    }
}
