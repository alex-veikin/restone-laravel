<?php

use Illuminate\Database\Seeder;

class AdminNavLinksTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_nav_links')->insert([
            [
                'uri'     => 'admin',
                'segment' => '',
                'title'   => 'Сводная информация',
            ],
            [
                'uri'     => 'admin/users',
                'segment' => 'users',
                'title'   => 'Пользователи',
            ],
            [
                'uri'     => 'admin/pages',
                'segment' => 'pages',
                'title'   => 'Страницы',
            ],
            [
                'uri'     => 'admin/cuisines',
                'segment' => 'cuisines',
                'title'   => 'Кухни',
            ],
            [
                'uri'     => 'admin/categories',
                'segment' => 'categories',
                'title'   => 'Категории',
            ],
            [
                'uri'     => 'admin/dishes',
                'segment' => 'dishes',
                'title'   => 'Блюда',
            ],
            [
                'uri'     => 'admin/cooks',
                'segment' => 'cooks',
                'title'   => 'Повара',
            ],
            [
                'uri'     => 'admin/orders',
                'segment' => 'orders',
                'title'   => 'Заказы',
            ],
            [
                'uri'     => 'admin/top-nav',
                'segment' => 'top-nav',
                'title'   => 'Верхнее меню',
            ],
            [
                'uri'     => 'admin/side-nav',
                'segment' => 'side-nav',
                'title'   => 'Боковое меню',
            ],
        ]);
    }
}
