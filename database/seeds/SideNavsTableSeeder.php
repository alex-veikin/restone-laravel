<?php

use Illuminate\Database\Seeder;

class SideNavsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('side_navs')->insert([
            [
                'type'          => 'page',
                'page_id'       => 1,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 0,
                'weight'        => 1,
            ],
            [
                'type'          => 'submenu',
                'page_id'       => null,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => 'Меню',
                'parent_id'     => 0,
                'weight'        => 2,
            ],
            [
                'type'          => 'page',
                'page_id'       => 2,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 2,
                'weight'        => 1,
            ],
            [
                'type'          => 'page',
                'page_id'       => 3,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 2,
                'weight'        => 2,
            ],
            [
                'type'          => 'page',
                'page_id'       => 4,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 2,
                'weight'        => 3,
            ],
            [
                'type'          => 'page',
                'page_id'       => 5,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 0,
                'weight'        => 3,
            ],
            [
                'type'          => 'page',
                'page_id'       => 6,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 0,
                'weight'        => 4,
            ],
            [
                'type'          => 'page',
                'page_id'       => 7,
                'url'           => null,
                'url_title'     => null,
                'submenu_title' => null,
                'parent_id'     => 0,
                'weight'        => 5,
            ],
        ]);
    }
}
