<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopNavsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_navs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('page_id')->nullable();
            $table->string('url')->nullable();
            $table->string('url_title')->nullable();
            $table->string('submenu_title')->nullable();
            $table->integer('parent_id')->default(0);
            $table->integer('weight')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('top_navs');
    }
}
